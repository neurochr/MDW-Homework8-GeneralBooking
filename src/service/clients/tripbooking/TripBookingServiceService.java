/**
 * TripBookingServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service.clients.tripbooking;

public interface TripBookingServiceService extends javax.xml.rpc.Service {
    public java.lang.String getTripBookingServicePortAddress();

    public service.clients.tripbooking.TripBookingService getTripBookingServicePort() throws javax.xml.rpc.ServiceException;

    public service.clients.tripbooking.TripBookingService getTripBookingServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
