/**
 * TripBookingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service.clients.tripbooking;

public interface TripBookingService extends java.rmi.Remote {
    public java.lang.String addTrip(java.lang.String destination, int capacity, int occupied) throws java.rmi.RemoteException;
    public java.lang.String addBooking(java.lang.String name, java.lang.String destination) throws java.rmi.RemoteException, service.clients.tripbooking.NoCapacityException, service.clients.tripbooking.NotAvailableException;
}
