package service.clients.tripbooking;

public class TripBookingServiceProxy implements service.clients.tripbooking.TripBookingService {
  private String _endpoint = null;
  private service.clients.tripbooking.TripBookingService tripBookingService = null;
  
  public TripBookingServiceProxy() {
    _initTripBookingServiceProxy();
  }
  
  public TripBookingServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initTripBookingServiceProxy();
  }
  
  private void _initTripBookingServiceProxy() {
    try {
      tripBookingService = (new service.clients.tripbooking.TripBookingServiceServiceLocator()).getTripBookingServicePort();
      if (tripBookingService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)tripBookingService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)tripBookingService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (tripBookingService != null)
      ((javax.xml.rpc.Stub)tripBookingService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public service.clients.tripbooking.TripBookingService getTripBookingService() {
    if (tripBookingService == null)
      _initTripBookingServiceProxy();
    return tripBookingService;
  }
  
  public java.lang.String addTrip(java.lang.String destination, int capacity, int occupied) throws java.rmi.RemoteException{
    if (tripBookingService == null)
      _initTripBookingServiceProxy();
    return tripBookingService.addTrip(destination, capacity, occupied);
  }
  
  public java.lang.String addBooking(java.lang.String name, java.lang.String destination) throws java.rmi.RemoteException, service.clients.tripbooking.NoCapacityException, service.clients.tripbooking.NotAvailableException{
    if (tripBookingService == null)
      _initTripBookingServiceProxy();
    return tripBookingService.addBooking(name, destination);
  }
  
  
}