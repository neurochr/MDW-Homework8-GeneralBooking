/**
 * FlightBookingServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service.clients.flightbooking;

public interface FlightBookingServiceService extends javax.xml.rpc.Service {
    public java.lang.String getFlightBookingServicePortAddress();

    public service.clients.flightbooking.FlightBookingService getFlightBookingServicePort() throws javax.xml.rpc.ServiceException;

    public service.clients.flightbooking.FlightBookingService getFlightBookingServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
