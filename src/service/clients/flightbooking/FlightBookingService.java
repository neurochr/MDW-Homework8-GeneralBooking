/**
 * FlightBookingService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package service.clients.flightbooking;

public interface FlightBookingService extends java.rmi.Remote {
    public java.lang.String addFlight(java.lang.String arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3) throws java.rmi.RemoteException;
    public java.lang.String addBooking(java.lang.String name, java.lang.String destination) throws java.rmi.RemoteException, service.clients.flightbooking.NotAvailableException;
}
