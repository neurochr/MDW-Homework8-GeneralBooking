package service.clients.flightbooking;

public class FlightBookingServiceProxy implements service.clients.flightbooking.FlightBookingService {
  private String _endpoint = null;
  private service.clients.flightbooking.FlightBookingService flightBookingService = null;
  
  public FlightBookingServiceProxy() {
    _initFlightBookingServiceProxy();
  }
  
  public FlightBookingServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initFlightBookingServiceProxy();
  }
  
  private void _initFlightBookingServiceProxy() {
    try {
      flightBookingService = (new service.clients.flightbooking.FlightBookingServiceServiceLocator()).getFlightBookingServicePort();
      if (flightBookingService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)flightBookingService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)flightBookingService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (flightBookingService != null)
      ((javax.xml.rpc.Stub)flightBookingService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public service.clients.flightbooking.FlightBookingService getFlightBookingService() {
    if (flightBookingService == null)
      _initFlightBookingServiceProxy();
    return flightBookingService;
  }
  
  public java.lang.String addFlight(java.lang.String arg0, java.lang.String arg1, java.util.Calendar arg2, java.util.Calendar arg3) throws java.rmi.RemoteException{
    if (flightBookingService == null)
      _initFlightBookingServiceProxy();
    return flightBookingService.addFlight(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String addBooking(java.lang.String name, java.lang.String destination) throws java.rmi.RemoteException, service.clients.flightbooking.NotAvailableException{
    if (flightBookingService == null)
      _initFlightBookingServiceProxy();
    return flightBookingService.addBooking(name, destination);
  }
  
  
}