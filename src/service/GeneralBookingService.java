package service;

import java.rmi.RemoteException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;

import service.clients.flightbooking.FlightBookingService;
import service.clients.flightbooking.FlightBookingServiceServiceLocator;
import service.clients.tripbooking.NoCapacityException;
import service.clients.tripbooking.TripBookingService;
import service.clients.tripbooking.TripBookingServiceServiceLocator;

@WebService
public class GeneralBookingService {
	private FlightBookingService flightBookingClient;
	private TripBookingService tripBookingClient;

	public GeneralBookingService() throws AxisFault, ServiceException {
		this.flightBookingClient = (new FlightBookingServiceServiceLocator()).getFlightBookingServicePort();
		this.tripBookingClient = (new TripBookingServiceServiceLocator()).getTripBookingServicePort();
	}

	@WebMethod(operationName = "makeBooking")
	@WebResult(name = "result")
	public String makeBooking(@WebParam(name = "type") String type, @WebParam(name = "name") String name,
			@WebParam(name = "destination") String destination) {
		switch (type.toLowerCase()) {
		case "flight":
			try {
				return this.flightBookingClient.addBooking(name, destination);
			} catch (service.clients.flightbooking.NotAvailableException e1) {
				return e1.getMessage1();
			} catch (RemoteException e1) {
				return e1.getMessage();
			}
		case "trip":
			try {
				return this.tripBookingClient.addBooking(name, destination);
			} catch (NoCapacityException e) {
				return e.getMessage1();
			} catch (service.clients.flightbooking.NotAvailableException e) {
				return e.getMessage1();
			} catch (RemoteException e) {
				return e.getMessage();
			}
		}
		return "Cannot create booking of type " + type + ". Only flights and trips can be booked.";
	}
}
